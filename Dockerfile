FROM node:latest

WORKDIR /usr/app

COPY package*.json /usr/app/
RUN npm install

COPY . /usr/app/

ENV PORT 5000
EXPOSE $PORT

CMD [ "npm", "start"]